readme.txt file for Computational AOM modeling project COMSOL file archive 10/25/2018

#DIET models
COMSOL file name: DIET_Hemispheres_enclosed.mph
Geometry: Hemispheres_enclosed
COMSOL file name: DIET_Spherical_layer.mph
Geometry: Spherical_layer
COMSOL file name: DIET_Half_half.mph
Geometry: Half_half
#HS2-MIET models
COMSOL file name: HS2_Hemispheres_enclosed.mph
Geometry: Hemispheres_enclosed
COMSOL file name: HS2_Spherical_layer.mph
Geometry: Spherical_layer
COMSOL file name: HS2_Half_half.mph
Geometry: Half_half
#SRB-MIET models with acetate as intermediate species
COMSOL file name: MIET_Acetate_Hemispheres_enclosed.mph
Geometry: Hemispheres_enclosed
COMSOL file name: MIET_Acetate_Spherical_layer.mph
Geometry: Spherical_layer
COMSOL file name: MIET_Acetate_Half_half.mph
Geometry: Half_half

COMSOL files are developed to simulate syntrophic interactions between the archaeal and bacterial cells mediating anaerobic oxidation of methane coupled with sulfate reduction include electron transfer through (1) the exchange of H2 or small organic molecules between methane oxidizing archaea and sulfate reducing bacteria (SRB-MIET), (2) the delivery of disulfide from methane oxidizing archaea to bacteria for disproportionation (HS2-MIET), and (3) direct interspecies electron transfer (DIET).